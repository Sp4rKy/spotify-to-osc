#!python3
"""
useful functions
"""

from logging import warning, info
import shelve

from spotipy.client import SpotifyException


# def dump_all_songs() -> None:
#    """"""
#    with shelve.open("songs.db") as s:
#        for song, data in s.items():
#            info(
#                f"{song}: {data['name']} ({data['tempo']}bpm / {data['tempo_confidence']} confidence) - {data['duration']}"
#            )


def update_library(
    sp, playlists: list[str], force: bool = False  # pylint: disable=invalid-name
) -> None:
    """Update cache with given playlists"""
    warning("Update playlists")
    for playlist in playlists:
        next_offset = 0
        while True:
            items = sp.playlist_items(playlist, offset=next_offset)
            next_offset += 100
            for i in range(len(items["items"])):
                track_id: str = items["items"][i]["track"]["id"]
                if track_id is None:
                    warning(f"Found empty track_id for {items['items'][i]}")
                    continue
                get_song_data_with_cache(sp, track_id, force)
                info(f"Add {track_id}")
            if not items["next"]:
                warning(f"Done playlist {playlist}")
                break


def get_song_data_with_cache(
    sp, track_id: str, local: bool = False  # pylint: disable=invalid-name
) -> dict | None:
    """Get a track data, from cache if possible"""
    with shelve.open("songs.db", writeback=True) as s:  # pylint: disable=invalid-name
        if track_id in s:  # and not force:
            info(f"{track_id} in cache. Use it")
            return s[track_id]
        if local:
            warning(
                f"Track {track_id} not in cache but --local has been set, won't change."
            )
            return None
        try:
            analysis = sp.audio_features([track_id])[0]
            return analysis
        except SpotifyException:
            warning(f"Can't find analysis for {track_id}")
            return None
