"""
Configuration items
"""
import os

ip: str = "127.0.0.1"
port: int = 7702

# Format of limit_config is:
# * key is the spotify features to filter on, ex: energy, acousticness, etc ...
#   See https://developer.spotify.com/documentation/web-api/reference/get-audio-features for available fields
# * value is a list of map of `maximum_value`: [list,of,osc,path]
#   maximum_value should be between 0 and 100 (percentage) for most of the features (except tempo).
#   As value from spotify are between 0 and 1, they are multiplied by 100
#   Each item of the list will be analysed, so you can configure different list of scenes for the same key
#
# You must make sure than all the osc paths are unique amongst all limit_config values
#
# Ex:
# Set a color scene based on enery, and a pattern scene based on tempo
#  { 'energy': [
#    {
#      50: ['/color/blue', '/color/green', '/color/white'], #If energy < 0.5, then picks one of the 3 scenes
#      80: ['/color/pink', '/color/yellow'],                #else if energy < 0.8, picks one of the 2 scenes
#      100: ['/color/red', '/color/scene/flash']               #else go for one of the red or flash scenes
#    }],
#    'tempo': [{                                             # AND
#      100: ['/pattern/beams/slow', '/pattern/wash/slow'],   # if tempo < 100, choose one of the slow scenes
#      200: ['/pattern/beams/fast', '/pattern/wash/fast'],   # else if < 200, choose one of the fast moving scenes
#   }]
#  }
#
# If you want to choose both colors and patterns based on energy:
#  { 'energy': [
#    {
#      50: ['/color/blue', '/color/green', '/color/white'], #If energy < 0.5, then picks one of the 3 scenes
#      80: ['/color/pink', '/color/yellow'],                #else if energy < 0.8, picks one of the 2 scenes
#      100: ['/color/red', '/color/scene/flash']               #else go for one of the red or flash scenes
#    },
#    {                                             # AND
#      60: ['/pattern/beams/slow', '/pattern/wash/slow'],   # if energy < 0.6, choose one of the slow scenes
#      100: ['/pattern/beams/fast', '/pattern/wash/fast'],   # else choose one of the fast moving scenes
#   }]
#  }


limit_config: dict[str, list[dict[int, list[str]]]] = {
    "energy": [
        {
            50: [
                "/color/blue",
                "/color/green",
                "/color/white",
            ],  # If energy < 0.5, then picks one of the 3 scenes
            80: [
                "/color/pink",
                "/color/yellow",
            ],  # else if energy < 0.8, picks one of the 2 scenes
            100: [
                "/color/red",
                "/color/scene/flash",
            ],  # else go for one of the red or flash scenes
        }
    ],
    "tempo": [
        {  # AND
            100: [
                "/pattern/beams/slow",
                "/pattern/wash/slow",
            ],  # if tempo < 100, choose one of the slow scenes
            200: [
                "/pattern/beams/fast",
                "/pattern/wash/fast",
            ],  # else if < 200, choose one of the fast moving scenes
        }
    ],
}

client_id: str = os.getenv("SPOTIFY_CLIENT_ID", "")
client_secret: str = os.getenv("SPOTIFY_CLIENT_SECRET", "")
scope: str = "user-library-read"
