#!python3
"""
Spotify to OSC : Trigger new light scenes through OSC on Spotify song change.
Do not require internet connection
"""

# Standard import
import sys
import random
from typing import NoReturn
from time import sleep
from logging import (
    basicConfig as logConfig,
    error,
    warning,
    info,
    debug,
    INFO,
    WARN,
    DEBUG,
)

# 3rd party import
import spotipy
import click
from dasbus.connection import InterfaceProxy, ObjectProxy, SessionMessageBus
from pythonosc.udp_client import SimpleUDPClient
from spotipy.oauth2 import SpotifyOAuth

# Local import
import config
from library import update_library, get_song_data_with_cache


class OSC:
    """
    Manage OSC protocol / connection
    """

    def __init__(self, ip, port):  # pylint: disable=invalid-name
        self.client: SimpleUDPClient = SimpleUDPClient(ip, port)  # Create client
        self.current_paths: list[str] = []

    def send(self, addr: str, value: float):
        """Send message to OSC bus"""
        self.client.send_message(addr, value)  # Send float message

    def send_all(self, paths):
        """Send all paths"""
        for path in paths:
            self.send(path, 1)
            sleep(0.2)

    def choose_random_scenes(self) -> list[tuple[str, int]]:
        """Choose new color / pattern randomly"""
        result: list[tuple[str, int]] = []
        new_paths: list[str] = []
        for _, attr_configs in config.limit_config.items():
            for attr_config in attr_configs:
                some_value: int = random.choice(list(attr_config.keys()))
                some_path: str = random.choice(attr_config[some_value])
                while (
                    some_path in self.current_paths
                ):  # Make sure the path wasn't in use
                    some_path: str = random.choice(attr_config[some_value])
                debug(f"Randomly chosen path {some_path}")
                result.append((some_path, 1))
        self.current_paths: list[str] = new_paths
        info(f"[OSC] Next random values: {new_paths}")
        return result

    def choose_next_scenes(self, song) -> list[tuple[str, int]]:
        """Choose new color / pattern based on new song"""
        result: list[tuple] = []
        new_paths: list[str] = []
        for attribute, attr_configs in config.limit_config.items():
            current_value: int | float = song.get(attribute)
            info(f"[{song['id']}] {attribute} = {current_value}")
            if isinstance(current_value, float) and current_value < 1:
                current_value = int(current_value * 100)

            for attr_config in attr_configs:
                config_maxs: list[int] = list(attr_config.keys())
                config_maxs.sort()
                for config_max in config_maxs:
                    debug(
                        f"[{song['id']}] {attribute} : {current_value} ? {config_max}"
                    )
                    if current_value < config_max:  # Find the upper limit
                        chosen_path: str = random.choice(
                            attr_config[config_max]
                        )  # Get a path
                        while (
                            chosen_path in self.current_paths
                        ):  # Make sure the path wasn't in use
                            chosen_path: str = random.choice(attr_config[config_max])

                        debug(
                            f"Current value {current_value} for {attribute} is < {config_max}. \
Will add path {chosen_path}"
                        )
                        result.append((chosen_path, 1))
                        new_paths.append(chosen_path)
                        break
        info(f"[OSC] Next values: {new_paths}")
        self.current_paths: list[str] = new_paths
        return result


class Spotify:
    """Manage Spotify interface"""

    def __init__(self):
        self.bus: SessionMessageBus = SessionMessageBus()
        self.current: str = ""
        self.sp: spotipy.client.Spotify | None = None  # pylint: disable=invalid-name

    def __read_current_song(self):
        data: InterfaceProxy | ObjectProxy = self.bus.get_proxy(
            "org.mpris.MediaPlayer2.spotify", "/org/mpris/MediaPlayer2"
        )
        track_data: str = data.Metadata["mpris:trackid"].get_string()  # pyright: ignore
        debug(track_data)
        return track_data.split("/")[-1]

    def wait_for_change(self, local: bool) -> dict | None:
        """Wait until song changes"""
        self.current = self.__read_current_song()
        while self.__read_current_song() == self.current:
            sleep(1)  # Wait one second
        new = self.__read_current_song()
        info(f"Got new song ({self.current} -> {new})")
        return self.get_song_data(new, local)

    def auth(self, scope, client_id, client_secret) -> None:
        """Auth against spotify"""
        self.sp = spotipy.Spotify(
            auth_manager=SpotifyOAuth(
                scope=scope,
                client_id=client_id,
                client_secret=client_secret,
                redirect_uri="http://localhost:18000",
            )
        )
        self.sp.current_user_saved_tracks()  # Just check we're auth

    def get_song_data(self, song_id: str, local: bool) -> dict | None:
        """Get data from song id"""
        return get_song_data_with_cache(self.sp, song_id, local)

    def update_playlists(self, playlists):
        """Update cache with some playlists"""
        update_library(self.sp, playlists)


@click.command()
@click.option(
    "-d", "--debug-log", default=False, is_flag=True, help="Add even more log"
)
@click.option("-v", "--verbose", default=False, is_flag=True, help="Add more log")
@click.option(
    "-p",
    "--playlist",
    default="",
    help="Ids of spotify playlists to add to cache, comma separated",
)
@click.option("-l", "--local", is_flag=True, help="Do not use spotify api at all")
@click.option(
    "-f",
    "--fallback",
    is_flag=True,
    help="If track data is not found, fallback to random scene choice.",
)
@click.option(
    "-c",
    "--configure",
    is_flag=True,
    help="Send all OSC paths, usefull with qlcplus profile wizard.",
)
def main(  # pylint: disable=too-many-branches,too-many-arguments
    debug_log: bool,
    verbose: bool,
    playlist: str,
    local: bool,
    fallback: bool,
    configure: bool,
) -> NoReturn:
    """
    Spotify to OSC
    Check spotify current song in almost realtime (every 1s by default)
    and take some actions when song changes
    """
    basic_format: str = "%(asctime)s - [%(levelname)s] %(message)s"
    if debug_log:
        logConfig(level=DEBUG, format=basic_format, datefmt="%Y-%m-%d %H:%M:%S")
    elif verbose:
        logConfig(level=INFO, format=basic_format, datefmt="%Y-%m-%d %H:%M:%S")
    else:
        logConfig(level=WARN, format=basic_format, datefmt="%Y-%m-%d %H:%M:%S")

    # validate config
    existing_paths: list[str] = []
    for _, v in config.limit_config.items():  # pylint: disable=invalid-name
        for item in v:
            for _, paths in item.items():
                if len(paths) < 2:
                    error(
                        "Must define at least 2 paths for every item in configuration"
                    )
                    sys.exit(1)

                for path in paths:
                    debug(f"Check path {path}")
                    if path in existing_paths:
                        error(f"Path {path} is duplicated")
                        sys.exit(1)
                    existing_paths.append(path)

    osc: OSC = OSC(config.ip, config.port)
    if configure:
        osc.send_all(existing_paths)
        sys.exit(0)

    spotify_inst: Spotify = Spotify()
    if not local:
        info("Need to auth on spotify")
        spotify_inst.auth(config.scope, config.client_id, config.client_secret)
    else:
        warning("Running in local mode")
    if playlist:
        if local:
            error("Playlist update requires internet, do not set -p and -l")
            sys.exit(1)
        else:
            info(f"Adding all songs from playlist {playlist} to cache")
            spotify_inst.update_playlists(playlist.split(","))
            sys.exit(0)

    while True:
        new_song = spotify_inst.wait_for_change(local)
        if new_song is not None:
            osc.choose_next_scenes(new_song)
        else:
            if fallback:
                info("No data found for new song, will fallback to random scene")
                osc.choose_random_scenes()
            else:
                warning(
                    "No data found for new song and fallback was not set, won't change anything."
                )


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
