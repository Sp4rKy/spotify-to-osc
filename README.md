# Spotify to OSC

I have some DMX features at home I use to control with the amazing QLC+ software.
I also use Spotify a lot to listen music in my house, and sometimes would like that my lights can change dynamically with Spotify song.

So here it is. This script will monitor Spotify for song change, and will send some OSC messages to QLC+ to trigger scene change.
To try matching lights with current song, you can configure the script so that it chooses which OSC path to send based on Spotify track analysis.

## Disclaimer

I´m not an OSC expert, far from that. Just used OSC instead of artnet as it was much faster & easy to configure, and I do like the named / readable paths

## Supported OSes

Right now, DBUS is required, so only Linux is available. Might do a fully-online version which uses only Spotify API laters.

## Offline capabilities

I like to depends on Internet as little as possible. Spotify (with premium account) can store song locally to play them while offline.
This software has been written with same goal in mind : be able to run even without internet. See `Offline mode` section for details

# Configure

First copy `config.sample.py` to `config.py` and edit the latest.

## General configuration

* `ip` : IP of your OSC server (like QLC+)
* `port`: Port of your OSC server
* `client_id`: Your Spotify API client_id, see https://developer.spotify.com/documentation/web-api . Default will be sourced from `SPOTIFY_CLIENT_ID` environment var
* `client_secret`: Your Spotify API client_secret, see https://developer.spotify.com/documentation/web-api . Default will be sourced from `SPOTIFY_CLIENT_SECRET` environment var
* `scope`: Spotify auth scope, comma separated. Default value should be fine unless you customize software

## Main filter configuration

The last item to configure is the `limit_config`. General structure is (in yaml like format):

```yaml
<spotify_feature_x>:
  - <max_value_1>:
      - <some_osc_path_1>
      - <some_osc_path_2>
    <max_value_2>:
      - <some_osc_path_3>
      - <some_osc_path_4>
  - <max_value_3>:
      - <some_osc_path_5>
      - <some_osc_path_6>
    <max_value_4>:
      - <some_osc_path_7>
      - <some_osc_path_8>
<spotify_feature_y>:
  - <max_value_5>:
    - <some_osc_path_9>
    - <some_osc_path_10>
  - <max_value_6>:
    - <some_osc_path_11>
    - <some_osc_path_12>
```

This example means that for every new song, it will:

* Check the spotify feature 'X' for this new song.
  * If the value is < `max_value_1`, will choose one of `some_osc_path_1` or `some_osc_path_2`
  * Else if the value is between `max_value_1` and `max_value_2`, will choose one of `some_osc_path_3` or `some_osc_path_4`
* Then check AGAIN the spotify feature 'X' as we have 2 items in this list. This allow to pick up multiple scenes (one for color, one for pattern for ex) based on only one feature
  * If the value is < `max_value_3`, will choose one of `some_osc_path_5` or `some_osc_path_6`
  * Else if the value is between `max_value_3` and `max_value_4`, will choose one of `some_osc_path_7` or `some_osc_path_8`
* Then, check spotify feature Y:
  * If the value is < `max_value_5`, will choose one of `some_osc_path_9` or `some_osc_path_10`
  * Else if the value is between `max_value_5` and `max_value_6`, will choose one of `some_osc_path_11` or `some_osc_path_12`

So, every time the song changes, 3 new scenes will be triggered based on 2 spotify features.

### Spotify features

You can find the list of featuers available here :  https://developer.spotify.com/documentation/web-api/reference/get-audio-features

The following should be usuable:

* acousticness
* danceability
* energy
* instrumentalness
* key
* liveness
* loudness
* mode
* speechiness
* tempo
* valence

Note: configuration only accept integers. It means that for all values which returns float between 0 and 1 (most of them in fact), script will convert them to percentage.
So a `50` for `energy` in configuration means `0.5` from Spotify API.

### Requirements

As the random choice is not really fully random (it retries if the new scene is the same as for last song ), you MUST set at least 2 path for every setting / section.

Also, each path must be unique in configuration.

## QLC+ Configuration

This tool should work with any software supporting OSC. But I do use QLC+ so let's give some hints to configure it.

1. In input/output choose a free universe (it is recommended to use different universes for inputs/feedback and outputs)
2. In Mapping tab choose one of the OSC line (either 127.0.0.1 if you're running spotify on the same computer, or the IP of your ethernet / wifi card) and check Input
3. In Profile tab click the `+` on top right to add a new profile, and name it as you would, then choose `Type` OSC
4. In second tab (Channels) click on the wizard icon, then on OK and keep it like that

This is where the tool can help. Assuming your configuration (`ip` and `port`) are correct, run the tool (see the Usage requirements before) with the `-c` option:
```
$ pipenv run main.py -c
```

This option do not require Spotify auth. It will send all the OSC paths found in configuration to the `ip:port` configured.
If everything is configured correctly, you should see all your paths appearing in QLC+.

5. Stop wizard
6. Customize channel type if needed
7. Save you profile

you should now be able to assign channels to your buttons / page next / prev in VC.

# Usage

## Requirements

Python 3.10 is required.
You should install required dependencies from Pipfile. Usage of `pipenv` is recommended:
```sh
$ pipenv install
```

## Usage and options

Basic usage is as simple as:

```sh
$ export SPOTIFY_CLIENT_ID=xxx
$ export SPOTIFY_CLIENT_SECRET=yyy
$ pipenv run main.py
```

Without more option, nothing will be print unless there is an error / issue.

Available options are:

```sh
$ pipenv run python3 ./main.py --help
Usage: main.py [OPTIONS]

  Spotify to OSC Check spotify current song in almost realtime (every 1s by
  default) and take some actions when song changes

Options:
  -d, --debug-log      Add even more log
  -v, --verbose        Add more log
  -p, --playlist TEXT  Ids of spotify playlists to add to cache, comma
                       separated
  -l, --local          Do not use spotify api at all
  -f, --fallback       If track data is not found, fallback to random scene
                       choice.
  -c, --configure      Send all OSC paths, usefull with qlcplus profile
                       wizard.
  --help               Show this message and exit.
```

As you guessed, `-v` will increase log level to `info`, and `-d` to `debug`.

`info` will print interesting information about song change detection and osc choices, like:

```sh
[INFO] Got new song (5S4aYQAJOwJMAamANWlICO -> 3jQyadhLTxpxadQlkFh2b8)
[WARNING] Track 3jQyadhLTxpxadQlkFh2b8 not in cache but --local has been set, won't change.
[INFO] No data found for new song, will fallback to random scene
[DEBUG] Randomly chosen path /color/yellow
[DEBUG] Randomly chosen path /pattern/beams/slow
[INFO] [OSC] Next random values: [('/color/yellow', 1), ('/pattern/beams/slow', 1)]
```

`debug` will also print every check (so 1 line per second), and the whole spotify API outputs. It will help to diagnose what happens, like:

```sh
[DEBUG] https://api.spotify.com:443 "GET /v1/audio-features/?ids=6i81qFkru6Kj1IEsB7KNp2 HTTP/1.1" 200 None
[DEBUG] RESULTS: {'audio_features': [{'danceability': 0.473, 'energy': 0.348, 'key': 0, 'loudness': -11.1, 'mode': 1, 'speechiness': 0.0288, 'acousticness': 0.721, 'instrumentalness': 7.42e-05, 'liveness': 0.0823, 'valence': 0.283, 'tempo': 107.819, 'type': 'audio_features', 'id': '6i81qFkru6Kj1IEsB7KNp2', 'uri': 'spotify:track:6i81qFkru6Kj1IEsB7KNp2', 'track_href': 'https://api.spotify.com/v1/tracks/6i81qFkru6Kj1IEsB7KNp2', 'analysis_url': 'https://api.spotify.com/v1/audio-analysis/6i81qFkru6Kj1IEsB7KNp2', 'duration_ms': 215853, 'time_signature': 4}]}
[INFO] [6i81qFkru6Kj1IEsB7KNp2] energy = 0.348
[DEBUG] [6i81qFkru6Kj1IEsB7KNp2] energy : 34 ? 50
[DEBUG] Current value 34 for energy is < 50. Will add path /color/green
[INFO] [6i81qFkru6Kj1IEsB7KNp2] tempo = 107.819
[DEBUG] [6i81qFkru6Kj1IEsB7KNp2] tempo : 107.819 ? 100
[DEBUG] [6i81qFkru6Kj1IEsB7KNp2] tempo : 107.819 ? 200
[DEBUG] Current value 107.819 for tempo is < 200. Will add path /pattern/wash/fast
[INFO] [OSC] Next values: ['/color/green', '/pattern/wash/fast']
```

`debug` mode *WILL*  print your Spotify credentials, be careful if you need to share logs.

# Local mode

As I expect this script could be used without internet, a set of features are available to achieve this.

## Cache

A local cache is build every time a song is fetched. If the song is then checked again, script first looks in cache before trying to call Spotify API.

Cache is stored in the `songs.db` file.

You can prefill the cache while you have internet connection. If you know which playlists will be played, just use the `-p` option:

```sh
$ pipenv run python3 ./main.py -p <playlist_id>,<another_playlist_id>
[WARNING] Update playlists
[WARNING] Done playlist <playlist_id>
[WARNING] Done playlist <another_playlist_id>
```

## Local mode

With `-l` option, you can force local mode, so no requests to spotify are sent (and you don't need to configure spotify credentials).

### Fallback to random option

By default, if a song is not found in cache and you asked for local mode, no change will happen.
Using `-f` option you can ask the script to pick a random path for every combination set in configuration.

